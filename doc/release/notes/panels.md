AEVA now includes ParaView's information, statistics, light, and memory
inspection panels. You can also set the `PV_DEBUG_LEAKS_VIEW` environment
variable when running aeva to debug memory leaks, although this feature
is currently untested.
