################################################
aeva: Annotation and Exchange of Virtual Anatomy
################################################
Version: |release|

.. highlight:: c++

.. role:: cxx(code)
   :language: c++

.. raw:: html

    <iframe src="https://drive.google.com/file/d/1YgB2DD_gz6-7qmMpm6f68oQzvXeLuPYp/preview" width="640" height="360"></iframe>

Contents:

.. toctree::
   :maxdepth: 4

   userguide/index.rst
   tutorials/index.rst

##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
