#!/bin/sh
#
# This script generates PNG images in various sizes and
# formats from several SVG files.
# It only works on macos with
#   + Xcode installed (for iconutil);
#   + python and the PIL module are installed; and
#   + Inkscape is installed.

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $self_dir

INKSCAPE=/Applications/Inkscape.app/Contents/MacOS/inkscape

# Make splash and about images for all platforms:
${INKSCAPE} -z -C -w 512 -o splash.png splash.svg
${INKSCAPE} -z -C -w 512 -o about.png about.svg

# Make linux icons
${INKSCAPE} -z -C -w 22 -h 22 -o aeva-22x22.png aeva-logo.svg
${INKSCAPE} -z -C -w 32 -h 32 -o aeva-32x32.png aeva-logo.svg
${INKSCAPE} -z -C -w 96 -h 96 -o aeva-96x96.png aeva-logo.svg
${INKSCAPE} -z "--export-area=0:-15.3:200:230.6" -w 137 -h 158 -o aeva-137x158.png aeva-logo.svg

# Make macos iconset:
# Note that aeva-logo-macos.svg has a smaller corner radius
# and additional, nearly-transparent padding to match the
# native OS style.
mkdir -p aeva.iconset
for rez in 16 32 128 256 512; do
  ${INKSCAPE} -z -C -w ${rez} -h ${rez} -o aeva.iconset/icon_${rez}x${rez}.png aeva-logo-macos.svg
  ${INKSCAPE} -z -C -w $((rez*2)) -h $((rez*2)) -o aeva.iconset/icon_${rez}x${rez}@2x.png aeva-logo-macos.svg
done
iconutil -c icns -o aeva.icns aeva.iconset
mv aeva.icns ..
rm -rf aeva.iconset

# Make windows icon:
${INKSCAPE} -z -C -w 256 -h 256 -o aeva-256x256.png aeva-logo.svg
cat > genico.py << EOF
from PIL import Image
filename = r'aeva-256x256.png'
img = Image.open(filename)
img.save('../aeva.ico')
EOF
python genico.py
rm genico.py aeva-256x256.png
